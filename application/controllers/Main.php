<?php

class Main extends CI_Controller {

  public function __construct()
	{
		parent::__construct();

		// $this->load->library('betfair');
		// $this->output->enable_profiler(TRUE);
    $this->load->helper('url_helper');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('main/index');
    $this->load->view('templates/footer');
  }
}
