<?php

class Mobile extends CI_Controller {

  public function __construct()
	{
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('user_model');
    $this->load->model('shops_model');
  }

  public function index()
  {
    redirect('/mobile/home');

    // if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
    //     redirect('/mobile/login');
    // }
    // $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    // $offer = $this->shops_model->getMyLastOffer($shop['shop_id']);
    // $data = array(
    //   'shop' => $shop,
    //   'offer' => $offer
    // );
    // $this->load->view('templates/header_mobile');
    // $this->load->view('mobile/last_offer', $data);
    // $this->load->view('templates/footer_mobile');
  }

  public function offer2($offer_id)
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $offer = $this->shops_model->getOffer($offer_id);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'offer' => $offer
    );
    $this->load->view('templates/header_mobile');
    $this->load->view('mobile/offer', $data);
    $this->load->view('templates/footer_mobile');
  }

  public function offers2($type)
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $types = array('expired','active','scheduled','deleted');
    $next_type = $type==0? 1:0;
    $next_status = $type==0? 'Active':'Expired';
    $status = $types[$type];
    $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    $offers = $this->shops_model->getMyOffers($this->session->userdata('userId'), $status);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'status' => $status,
      'offers' => $offers,
      'shop' => $shop,
      'next_type' => $next_type,
      'next_status' => $next_status
    );
    $this->load->view('templates/header_mobile');
    $this->load->view('mobile/offers', $data);
    $this->load->view('templates/footer_mobile');
  }

  public function home()
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $this->load->view('mobile/screen1');
  }

  public function top1()
  {
    $data = array(
      'shop_name' => $this->session->userdata('shop_name'),
      'first_name' => $this->session->userdata('first_name')
    );
    $this->load->view('mobile/frames/top1', $data);
  }

  public function mid1a()
  {
    $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    $offer = $this->shops_model->getMyLastOffer($shop['shop_id']);
    $data = array(
      'shop' => $shop,
      'offer' => $offer
    );
    $this->load->view('mobile/frames/mid1a', $data);
  }

  public function mid2($type)
  {
    $status = $type==1? 'active':'expired';
    $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    $offers = $this->shops_model->getMyOffers($this->session->userdata('userId'), $status);

    if($type==1) {
      $title = count($offers) . " - Active Offers";
    }
    else{
      $title = "Expired Offers";
    }
    $data = array(
      'title' => $title,
      'shop' => $shop,
      'offers' => $offers
    );
    $this->load->view('mobile/frames/mid2', $data);
  }

  public function mid4bottom($offer_id)
  {
    $offer = $this->shops_model->getOffer($offer_id);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'offer' => $offer,
      'offer_id' => $offer_id
    );
    $this->load->view('mobile/frames/mid4bottom', $data);
  }

  public function mid5($offer_id)
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $offer = $this->shops_model->getOffer($offer_id);
    $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    $sales = $this->shops_model->getMySalesForOffer($shop['shop_id'], $offer_id);
    $offer_sku = $this->shops_model->getOfferSku($offer_id);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'offer' => $offer,
      'offer_id' => $offer_id,
      'sales' => $sales,
      'offer_sku' => $offer_sku,
      'remain' => (float)$offer['cond_value'] - (float)$sales['total']
    );
    $this->load->view('mobile/frames/mid5', $data);
  }

  public function offer($offer_id)
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $offer = $this->shops_model->getOffer($offer_id);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'offer' => $offer,
      'offer_id' => $offer_id
    );
    $this->load->view('mobile/Screen4', $data);
  }

  public function offer_detail($offer_id)
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $offer = $this->shops_model->getOffer($offer_id);
    // $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    // $sales = $this->shops_model->getMySalesForOffer($shop['shop_id'], $offer_id);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'offer' => $offer,
      'offer_id' => $offer_id,
    );
    $this->load->view('mobile/Screen5', $data);
  }

  public function offers_active()
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $status = 'active';

    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'status' => $status,
      'mid_title' => '1',
      'base' => 'base2'
    );
    $this->load->view('mobile/screen2', $data);
  }

  public function offers_expired()
  {
    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=5) {
        redirect('/mobile/login');
    }
    $status = 'expired';
    $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
    $offers = $this->shops_model->getMyOffers($this->session->userdata('userId'), $status);
    $data = array(
      'first_name' => $this->session->userdata('first_name'),
      'status' => $status,
      'offers' => $offers,
      'shop' => $shop,
      'mid_title' => '0',
      'base' => 'base3'
    );
    $this->load->view('mobile/screen2', $data);
  }

  public function base1()
  {
    $this->load->view('mobile/frames/base1');
  }

  public function base2()
  {
    $this->load->view('mobile/frames/base2');
  }

  public function base3()
  {
    $this->load->view('mobile/frames/base3');
  }

  public function base5($offer_id)
  {
    $this->load->view('mobile/frames/base5', array('offer_id'=>$offer_id));
  }

  public function login()
  {
    $data = array();
    if($this->input->post('password')) {
        // $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == true) {
            $con['returnType'] = 'single';
            $con['conditions'] = array(
                'user_id'=>$this->input->post('email'),
                'password' => $this->input->post('password'),
                // 'status' => '1'
            );
            $checkLogin = $this->user_model->shopUserLogin($con);
            if($checkLogin){
                $this->session->set_userdata('isUserLoggedIn',TRUE);
                $this->session->set_userdata('userId',$checkLogin['user_id']);
                $this->session->set_userdata('first_name',$checkLogin['first_name']);
                $this->session->set_userdata('userType',5);

                $shop = $this->shops_model->getMyShop($this->session->userdata('userId'));
                $this->session->set_userdata('shop_name',$shop['shop_name']);

                redirect('mobile');
            }else{
                $data['error_msg'] = 'Wrong email or password, please try again.';
            }
        }
    }
    // $this->load->view('templates/header_mobile');
    $this->load->view('mobile/login', $data);
    // $this->load->view('templates/footer_mobile');
  }

  public function logout(){
      $this->session->unset_userdata('isUserLoggedIn');
      $this->session->unset_userdata('userId');
      $this->session->sess_destroy();
      redirect('mobile/login');
  }
}
