<?php

class Shops extends CI_Controller {

  public function __construct()
	{
		parent::__construct();

    if(!$this->session->userdata('isUserLoggedIn') || !$this->session->userdata('type')!=1) {
    // if(!$this->session->userdata('isUserLoggedIn') && ) {
        redirect('users/login');
    }

    $this->load->model('shops_model');
  }

  public function index()
  {
    $data = array();
    $this->load->view('templates/header');
    $this->load->view('shops/index', $data);
    $this->load->view('templates/footer');
  }

  public function create_offer()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
       $offer = array(
         'offer_title' => $this->input->post('offer_title'),
         'offer_text' => $this->input->post('offer_text'),
         'cond_base ' => 'sales volume',
         'cond_value' => $this->input->post('cond_value'),
         'entry_date' => date('Y-m-d'),
         'start_date' => $this->input->post('startdate'),
         'end_date' => $this->input->post('enddate'),
         'condper_start' => $this->input->post('condper_start'),
         'condper_end' => $this->input->post('condper_end'),
         'description' => $this->input->post('desc'),
         'status' => 'active'
       );
       $sku_list = $this->input->post('cond_brands');
       $this->shops_model->insertOffer($offer, $sku_list);
    }

    $this->load->view('templates/header');
    $this->load->view('shops/create_offer');
    $this->load->view('templates/footer');
  }

  public function assign_offer()
  {
    $groups = $this->shops_model->getGroups();
    $offers = $this->shops_model->getOffers();
    $data = array(
      'groups' => $groups,
      'offers' => $offers
    );
    $this->load->view('templates/header');
    $this->load->view('shops/assign_offer', $data);
    $this->load->view('templates/footer');
  }

  public function assign_offers_save()
  {
    $offer_id = $this->input->get('offer_id');
    $groups = explode(',', $this->input->get('groups'));
    if($offer_id!='' && count($groups)>0)
    {
      $data = array(
        'offer_id' => $offer_id,
        'groups' => $groups
      );
      $this->shops_model->saveOfferAssignments($data);
    }
  }

  public function listShopsTypes()
  {
		$shops = $this->shops_model->getShopTypes();

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($shops, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function listZone()
  {
		$zone = $this->shops_model->getZone();

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($zone, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function getDistributors()
  {
		$distrib = $this->shops_model->getDistributors();

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($distrib, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function getCategories()
  {
		$categories = $this->shops_model->getCategories();

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($categories, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function getBrands()
  {
    $cat_ids = array();
    if($this->input->get('cat_ids')!='')
    {
      $cat_ids = explode(',', $this->input->get('cat_ids'));
    }
    $filter = array(
      'cat_ids' => $cat_ids
    );
    $brands = $this->shops_model->getBrands($filter);

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($brands, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function getSku()
  {
		$sku = $this->shops_model->getSku();

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($sku, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function listShops()
  {
    $shop_types = array();
    $zone_types = array();
    $distrib = array();
    $brands = array();
    $min_sales = 0;

    if($this->input->get('shop_types')!='')
    {
      $shop_types = explode(',', $this->input->get('shop_types'));
    }
    if($this->input->get('zone_types')!='')
    {
      $zone_types = explode(',', $this->input->get('zone_types'));
    }
    if($this->input->get('distributors')!='')
    {
      $distrib = explode(',', $this->input->get('distributors'));
    }
    if($this->input->get('brands')!='')
    {
      $brands = explode(',', $this->input->get('brands'));
    }
    if($this->input->get('min_sales')!='')
    {
      $min_sales = (int)$this->input->get('min_sales');
    }

    $filter = array(
      'shop_types' => $shop_types,
      'zone_types' => $zone_types,
      'distrib' => $distrib,
      'brands' => $brands,
      'min_sales' => $min_sales,
      'from_date' => $this->input->get('from_date'),
      'to_date' => $this->input->get('to_date')
    );
    if($filter['min_sales']==0 && $filter['from_date']=='' && $filter['to_date']=='' && count($filter['brands'])==0)
    {
      $shops = $this->shops_model->getShops($filter);
    }
    else
    {
      $shops = $this->shops_model->getShopsWithSales($filter);
    }

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($shops, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }

  public function saveGroup()
  {
    $response = array();
    $group = array(
      'group_name' => $this->input->get('group_name'),
      'shops' => explode(',', $this->input->get('group_shops'))
    );
    if($group['group_name']!='' && count($group['shops'])>0)
    {
      $group_id = $this->shops_model->insertGroup($group);
      $response = array(
        'group_id' => $group_id
      );
    }
    else
    {
      $response = array(
        'error' => 'unable to save group'
      );
    }

    $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
  }
}
