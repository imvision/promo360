<!DOCTYPE html>
<html lang="en">

	<head>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <title>Shops</title>

	    <!-- Bootstrap Core CSS -->
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
			<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

			<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/datepicker.css'?>" />
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

			<!-- jQuery -->
	    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

      <!-- <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->

	    <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

			<!-- Include Date Range Picker -->
      <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap-datepicker.js'?>"></script>

      <!-- Vue JS -->
      <script src="<?php echo base_url(); ?>assets/js/vue.min.js"></script>

			<script type="text/javascript">
				var base_url = '<?php echo base_url();?>';
			</script>

			<!-- App Logic -->
      <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	</head>
<body>

	<!-- Navigation -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo site_url('/'); ?>">Promo360</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li><a href="<?php echo base_url('/shops');?>">Shops</a></li>
									<li><a href="<?php echo base_url('/shops/create_offer');?>">Create Offer</a></li>
									<li><a href="<?php echo base_url('/shops/assign_offer');?>">Assign Offers</a></li>
              </ul>
							<ul class="nav navbar-nav pull-right">
								<li><a href="<?php echo base_url('/users/logout');?>">Logout</a></li>
							</ul>
          </div>
          <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
	</nav>

  <div class="container">
    <div class="row">
