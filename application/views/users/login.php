<!DOCTYPE html>
<html lang="en">

	<head>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <title>Login | Promo360</title>

	    <!-- Bootstrap Core CSS -->
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
			<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="<?php echo base_url(); ?>assets/css/signin.css" rel="stylesheet">

	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

			<!-- jQuery -->
	    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

      <!-- <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->

	    <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

			<script type="text/javascript">
				var base_url = '<?php echo base_url();?>';
			</script>
	</head>
  <body>
    <div class="container">

     <form class="form-signin" method="post">
       <h2 class="form-signin-heading">Please sign in</h2>

       <?php if(isset($error_msg) && $error_msg!=''):?>
       <p class="bg-danger"><?php echo $error_msg?></p>
       <?php endif;?>

       <label for="inputEmail" class="sr-only">Email address</label>
       <input type="email" name='email' id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
       <label for="inputPassword" class="sr-only">Password</label>
       <input type="password" name='password' id="inputPassword" class="form-control" placeholder="Password" required>
       <!-- <div class="checkbox">
         <label>
           <input type="checkbox" value="remember-me"> Remember me
         </label>
       </div> -->
       <button class="btn btn-lg btn-primary btn-block" name="loginSubmit" type="submit">Sign in</button>
     </form>

   </div> <!-- /container -->
 </body>
</html>
