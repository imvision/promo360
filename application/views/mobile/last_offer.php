<div class="row">
  <div class="col-md-12">
     <div class="heading">
        <h4> <?php echo $shop['first_name'] ?>
        </h4>
        <div class="shp">
           <h4> <?php echo $shop['shop_name'] ?> </h4>
           <div class="out"> <span class="glyphicon glyphicon-remove"></span></div>
        </div>
     </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
     <div class="border">
        <div class="heading2">
           <h2> Last offer </h2>
        </div>
        <br>
        <div class="border1">
           <div class="heading3">
              <!-- <h3> Loading new offers temp</h3> -->
              <?php if(count($offer)>0):?>
                <h3>
                  <a href="<?php echo base_url() . 'mobile/offer/'. $offer['offer_id'] ?>">
                    <?php echo $offer['offer_title'] ?>
                  </a>
                </h3>
              <?php else:?>
                <h3>No offers available</h3>
              <?php endif;?>
           </div>
        </div>
        <br>
     </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-xs-6">
     <a class="button" href="<?php echo base_url().'mobile/offers/1' ?>">Active</a>
     <a class="button" href="<?php echo base_url().'mobile/offers/0' ?>">Expired</a>
  </div>
  <div class="col-xs-6">
     <input type="button" class="button" value="Availed">
     <input type="button" class="button" value="Favorites">
  </div>
</div>
