<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Main</title>
<style type="text/css">
<!--
@import url("<?php echo base_url(); ?>assets/css/mobile-new.css");
.style2 {
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
}
.style5 {
	font-weight: bold;
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
}
.style7 {font-size: 10px}
.style25 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666666;
}
.style28 {font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FF9900; }
.style33 {font-size: 10px}
a:link {
	color: #666666;
	text-decoration: none;
}
a:visited {
	color: #666666;
	text-decoration: none;
}
a:hover {
	color: #666666;
	text-decoration: none;
}
a:active {
	color: #666666;
	text-decoration: none;
}
-->
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td bgcolor="#4682B4" class="style7"><div align="center"><span class="style5"><?php echo $title ?> </span></div></td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="8" cellspacing="8">
  <?php foreach($offers as $offer):?>
  <tr>
    <td width="70%" align="left" valign="top">
      <span class="style25">
        <a href="<?php echo base_url(). 'mobile/offer/' .$offer['offer_id']?>" target="_top">
          <?php echo $offer['label'] ?>        </a>      </span>    </td>
    <td width="30%" align="right" valign="top"><span class="style28">
      <?php echo date('M d,y', strtotime($offer['end']))?>
      </span></td>
  </tr>
  <?php endforeach;?>
</table>
<p class="style2">&nbsp;</p>
</body>
</html>
