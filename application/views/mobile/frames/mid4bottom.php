<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Main</title>
<style type="text/css">
<!--
/*@import url("file:///css/style.css");*/
@import url("<?php echo base_url(); ?>assets/css/mobile-new.css");
.style2 {
	font-size: 15px;
	font-family: Arial, Helvetica, sans-serif;
}
.style3 {
	font-size: 12px;
	color: #333333;
	font-family: Arial, Helvetica, sans-serif;
}
.style8 {font-size: 12px}
.style9 {color: #FF6600}
.style10 {
	color: #0099CC;
	width: 95%;
	font-style: normal;
}
.style12 {font-size: 12px; color: #666666; }
-->
</style>
</head>

<body>
<p align="center" class="style2"><span class="style10"><?php echo $offer['offer_title']?></span></p>
<p align="center" class="style3"><span class="style8"><?php echo $offer['description'] ?></span></p>
<p align="center" class="style3">
  <?php if(strtotime(date('Y-m-d')) < strtotime($offer['end_date'])):?>
</p>
<table width="96%" height="30%" border="0" align="center" cellpadding="0" cellspacing="5">
  <tr>
    <td height="50%" align="center" valign="bottom"><p class="col-xs-6">
      <span class="space4 style2 style9"><span class="style10"><span class="col-xs-6 style16">
      <a href="<?php echo base_url().'mobile/offer_detail/'.$offer_id ?>" target="_top">
        <input name="chances" type="button" class="style10" id="chances" value="MY CHANCES" />
      </a>
      </span></span></span></p>
      <p class="col-xs-6">&nbsp;</p>
      <p class="col-xs-6"><span class="space4 style2 style9"><span class="style9">OFFER ACTIVE</span> <br />
    <span class="style12">till</span> <?php echo date('M d,y',strtotime($offer['end_date'])) ?> </span></p>
    <p class="col-xs-6">&nbsp;</p>
    </td>
  </tr>
</table>
<?php else: ?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="5">
  <tr>
    <td width="96%" height="50%" align="center" valign="bottom">
      <p class="col-xs-6">&nbsp;</p>
      <p class="col-xs-6"><span class="space4 style2 style9"><span class="style9">OFFER EXPIRY</span> <br />
      <span class="style12">on</span> <?php echo $offer['end_date'] ?> </span></p>
      <p class="col-xs-6">&nbsp;</p>
    </td>
  </tr>
</table>
<?php endif;?>
</body>
</html>
