<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Main</title>
<style type="text/css">
<!--
@import url("<?php echo base_url(); ?>assets/css/mobile-new.css");
.style2 {
	font-size: 15px;
	font-family: Arial, Helvetica, sans-serif;
}
.style3 {
	font-size: 15px;
	color: #333333;
	font-family: Arial, Helvetica, sans-serif;
}
.style9 {color: #FF6600}
.style10 {color: #0099CC}
.style12 {font-size: 15px; color: #666666; }
.style13 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body>
<p align="center" class="style2"><span class="style12">Target is  Sales   of AED</span>
	<span class="style9"> <?php echo number_format($offer['cond_value']) ?></span>
	<span class="style12"> on <br />
  </span>
	<span class="style10">
		<?php if(count($offer_sku)>0):?>
		<?php echo $offer_sku[0]['cat'];?>
		 -
		<?php
		$sku_str = '';
		foreach($offer_sku as $sku){
			$sku_str .= ' '.$sku['sku'].',';
		}
		echo rtrim($sku_str, ',');
		?>
		<?php endif;?>
	</span>
	<span class="style12">
	<br /> before <?php echo date('M d,y', strtotime($offer['end_date']))?> </span></p>
<p align="center" class="style2">&nbsp;</p>
<table width="96%" height="40%" border="2" align="center" cellpadding="5" cellspacing="0" bordercolor="#FF9900" bgcolor="#FFFFFF">
  <tr>
    <td height="50%" align="center" valign="middle" bgcolor="#FFFFFF"><p class="col-xs-6"><span class="space4 style2 style9"><span class="style10">      SALES REMAINING </span> <br />
          <span class="style12">AED  </span> <?php echo number_format($remain) ?><br />
    </span><span class="space4 style2 style9">

		<?php $d1 = date_create(date('Y-m-d')); $d2 = date_create($offer['end_date']); $intv = date_diff($d1, $d2); ?>
			<span class="style12">in <?php echo $intv->format('%a')?> days</span>
			</span></p>
      <p class="col-xs-6"><span class="space4 style2 style9"><span class="style12">Current Sale : <?php echo number_format( $sales['total'])?><br />
        <br />
      </span></span></p>
    </td>
  </tr>
</table>
</body>
</html>
