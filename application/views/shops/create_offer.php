<h2>Create new offer</h2>
<div class="row" id="create_offer_form">
  <form class="form-horizontal" method="post">
    <div class="form-group hide">
      <label for="offer_title" class="col-sm-2 control-label">Offer</label>
      <div class="col-sm-3">
        <input type="text" class="form-control hide" name="offer_text"  id="offer_text" placeholder="Offer">
      </div>
    </div>
    <div class="form-group hide">
      <label for="offer_title" class="col-sm-2 control-label">On</label>
      <div class="col-sm-3">
        <select v-model="categories_selected" class="form-control hide">
          <option v-for="type in categories_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
      <div class="col-sm-3 hide">
        <select name='cond_brand_id' v-model="brands_selected" class="form-control hide">
          <option v-for="type in brands_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
      <div class="col-sm-3">
        <select id="offer_skus" name="off_skus" class="form-control hide" multiple disabled>
          <option></option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">When</label>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Sales volume is</label>
      <div class="col-sm-2">
        <input type="number" name="cond_value" class="form-control" id="cond_value" placeholder="0">
      </div>
      <label class="col-sm-2">or higher</label>
    </div>
    <div class="form-group">
      <label for="offer_title" class="col-sm-2 control-label">Of</label>
      <div class="col-sm-3">
        <select v-model="cond_category" name="category" class="form-control">
          <option v-for="type in categories_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
      <div class="col-sm-3">
        <select v-model="cond_brands_selected" name="cond_brands[]" class="form-control" multiple>
          <option v-for="type in cond_brands" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
      <div class="col-sm-3">
        <select id="offer_skus" name="skus" class="form-control hide" multiple disabled>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">For the period</label>
      <div class="col-sm-2">
        <input type="text" name="startdate" class="input form-control datepicker" />
      </div>
      <label class="col-sm-1">to</label>
      <div class="col-sm-2">
        <input type="text" name="enddate" class="input form-control datepicker" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">OFFER NAME * </label>
      <div class="col-sm-2 ">
        <input name="offer_title" type="text" class="form-control" id="offer_label" value="Visible to Shop" size="50">
      </div>
      <div class="col-sm-2 hide">
        <input type="text" name="condper_start" class="input form-control datepicker" />
      </div>
      <label class="col-sm-1 hide">to</label>
      <div class="col-sm-2 hide">
        <input type="text" name="condper_end" class="input form-control datepicker" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">OFFER DESCRIPTION * </label> 
      <div class="col-sm-4">
        <textarea name="desc" cols="45" rows="3" class="form-control" id="cond_value">Visible to Shop</textarea>
      </div>
      <div form-group='col-sm-1'>
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
  var new_offer = new Vue({
    el: '#create_offer_form',
    data: {
      categories_list: [],
      categories_selected: '',
      brands_list: [],
      brands_selected: [],

      cond_category: '',
      cond_brands: [],
      cond_brands_selected: []
    },
    created: function() {
      shopsApi.listCategories(this.categories_list)
    },
    methods: {
      listCategories: function() {
        var self = this
        jQuery.get(base_url+'shops/getCategories', function(data) {
          self.categories_list = data
        })
      },
      listBrands: function() {
        var self = this
        var param = jQuery.param({
          'cat_ids': this.categories_selected
        })
        jQuery.get(base_url+'shops/getBrands?'+param, function(data) {
          self.brands_list = data
        })
      },
    },
    watch: {
      categories_selected: function(val) {
        this.brands_list = []
        shopsApi.listBrands(this.categories_selected, this.brands_list)
      },
      cond_category: function(val) {
        this.cond_brands = []
        shopsApi.listBrands(this.cond_category, this.cond_brands)
      }
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.datepicker').datepicker({
      'autoclose': true,
      'format': 'yyyy-mm-dd'
    })
  })
</script>
