<h4>Offer Assignment</h4>
<div class="row" id="create_offer_form">
  <p id="flash_msg"></p>
  <form class="form-horizontal" onsubmit="event.preventDefault();">
    <div class="row">
      <div class="col-sm-3">
        <label for="offer_title" class="control-label">Select Offer</label>
      </div>
      <div class="col-sm-3">
        <label for="offer_title" class="control-label">Select Groups</label>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <select class="form-control" v-model='offer_id'>
          <?php foreach($offers as $offer): ?>
          <option value="<?php echo $offer['offer_id']?>">
            <?php echo $offer['offer_title']?>
          </option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-sm-3">
        <p>
          <select class="form-control" v-model='groups_list' multiple>
            <?php foreach($groups as $group): ?>
            <option value="<?php echo $group['group_id']?>">
              <?php echo $group['group_name']?>            </option>
            <?php endforeach; ?>
          </select>
        </p>
        <p>&nbsp;  </p>
      </div>
    </div>
    <div class="row form-group col-md-6">
      <button @click='assign_offer' class="btn btn-primary pull-right">Assign</button>
    </div>
  </form>
</div>

<script type="text/javascript">
var assign_offer = new Vue({
  el: '#create_offer_form',
  data: {
    offer_id: '',
    groups_list: [],
  },
  methods: {
    assign_offer: function() {
      if(this.offer_id=='') {
        jQuery('#flash_msg').text('Select an offer');
        return
      }
      if(this.groups_list.length==0) {
        jQuery('#flash_msg').text('Select groups');
        return
      }
      var params = jQuery.param({
        'offer_id': this.offer_id,
        'groups': this.groups_list.join(',')
      });
      jQuery.get(base_url+'shops/assign_offers_save?'+params, function(data) {
        jQuery('#flash_msg').text('Offer assigned successfully');
      });
    }
  }
})
</script>
