<h2>Select Shops</h2>
<div class="row" id="shops">
  <div class="row">
    <div class="col-md-4">
      <div class="checkbox">
        <label>
          <input type="checkbox" v-model="shop_types_filter"> Type
        </label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="shopTypesSelectAll()">Select All</a>
      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="shopTypesClearAll()">Clear All</a>
      </div>

      <div class="col-md-9">
        <select v-model="shop_types_selected" v-bind:disabled="! shop_types_filter" class="form-control" multiple>
          <option v-for="type in shop_types_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
    </div>

    <div class="col-md-4">
      <div class="checkbox">
        <label>
          <input type="checkbox" v-model="zone_filter"> Zone
        </label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="zoneSelectAll()">Select All</a>
      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="zoneClearAll()">Clear All</a>
      </div>

      <div class="col-md-9">
        <select v-model="zone_selected" v-bind:disabled="! zone_filter" class="form-control" multiple>
          <option v-for="type in zone_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
    </div>

    <div class="col-md-4">
      <div class="checkbox">
        <label>
          <input type="checkbox" v-model="distrib_filter"> Distributors
        </label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="distribSelectAll()">Select All</a>
      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="distribClearAll()">Clear All</a>
      </div>

      <div class="col-md-9">
        <select v-model="distrib_selected" v-bind:disabled="! distrib_filter" class="form-control" multiple>
          <option v-for="type in distrib_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
    </div>
  </div>

  <hr />
  <div class="row">
    <p>Sales of AED <input type="number" class="input" v-model="min_sales"> of</p>
    <div class="col-md-4">
      <div class="checkbox">
        <label>
          <input type="checkbox" v-model="category_filter"> Category
        </label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="categoriesSelectAll()">Select All</a>
      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="categoriesClearAll()">Clear All</a>
      </div>

      <div class="col-md-9">
        <select v-model="categories_selected" v-bind:disabled="! category_filter" class="form-control" multiple>
          <option v-for="type in categories_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
    </div>

    <div class="col-md-4">
      <div class="checkbox">
        <label>
          <input type="checkbox" v-model="brand_filter"> Brand
        </label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="brandSelectAll()">Select All</a>
      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="brandClearAll()">Clear All</a>
      </div>

      <div class="col-md-9">
        <select v-model="brands_selected" v-bind:disabled="! brand_filter" class="form-control" multiple>
          <option v-for="type in brands_list" v-bind:value="type.id">{{ type.text }}</option>
        </select>
      </div>
    </div>

    <div class="col-md-4">
      <div class="checkbox">
        <label></label>
      </div>

      <div class="form-group">
        <a href="#shop" v-on:click="skuSelectAll()"></a>      </div>
      <div class="form-group">
        <a href="#shop" v-on:click="skuClearAll()"></a>      </div>

      <div class="col-md-9"></div>
    </div>
<div class="clearfix"></div>
<br>
    <div class="row">
      <div class="form-group">
        <label class="col-sm-1 control-label">        From</label>
        <div class="col-sm-2">
          <input type="text" name="fromdate" class="input form-control fromdate" />
        </div>
        <label class="col-sm-1 control-label">        To</label>
        <div class="col-sm-2">
          <input type="text" name="todate" class="input form-control todate" />
          </div>
      </div>
    </div>

      <p><button v-on:click="listShops" class="btn btn-primary" class='' id="filterShops">Apply</button></p>
  </div>

  <hr />
  <h3>Shops List</h3>
  <div class="row col-md-12" id="shopsListBlock">
    <table class="table table-bordered table-responsive">
      <thead>
        <tr>
          <th>#</th>
          <th>Shop Name</th>
          <th>District</th>
          <th>Type</th>
          <th>Zone</th>
          <th>Distributor</th>
          <th>Info</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="shop in shopsList">
          <td><input type="checkbox" name="shopId[]" v-model="shop.selected"></td>
          <td>{{ shop.name }}</td>
          <td>{{ shop.district }}</td>
          <td>{{ shop.type }}</td>
          <td>{{ shop.zone }}</td>
          <td>{{ shop.distributor }}</td>
          <td>
            <a href="#shopsListBlock" v-on:click="showShopInfo(shop)">
              <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="row col-md-12">
    <button class="btn btn-primary" v-on:click="modalGroupName()">Save Group</button>
    </div>

  <!-- Shop info modal -->
  <div class="modal fade bs-shop-info-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            {{ shopPopup.name }}
          </h4>
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>Type</td>
              <td>{{ shopPopup.type }}</td>
            </tr>
            <tr>
              <td>Latitude</td>
              <td>{{ shopPopup.lat }}</td>
            </tr>
            <tr>
              <td>Longitude</td>
              <td>{{ shopPopup.lng }}</td>
            </tr>
            <tr>
              <td>Zone</td>
              <td>{{ shopPopup.zone }}</td>
            </tr>
            <tr>
              <td>Distributor</td>
              <td>{{ shopPopup.distributor }}</td>
            </tr>
            <tr>
              <td>District</td>
              <td>{{ shopPopup.district }}</td>
            </tr>
            <tr>
              <td>City</td>
              <td>{{ shopPopup.city }}</td>
            </tr>
            <tr>
              <td>State</td>
              <td>{{ shopPopup.state }}</td>
            </tr>
            <tr>
              <td>Country</td>
              <td>{{ shopPopup.country }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Group name input -->
  <div class="modal fade bs-group-name-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            Enter group name
          </h4>
        </div>
        <div class="modal-body">
          <input type='text' v-model='group_name' />
          <button v-on:click='groupSave()'>Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Goto offer page -->
  <div class="modal fade bs-offer-navigation-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
          </h4>
        </div> -->
        <div class="modal-body">
          <p>Do you want to assign offer to this group?</p>
          <button v-on:click='navigateToOffer()'>Yes</button>
          <button v-on:click='navigateToHome()'>No</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  // Shops
  var shops = new Vue({
    el: '#shops',
    data: {
      shopsList: [],
      shopPopup: {},
      min_sales: '',
      fromDate: '',
      toDate: '',

      shop_types_filter: true,
      zone_filter: true,
      distrib_filter: true,
      category_filter: true,
      brand_filter: true,
      sku_filter: false,

      shop_types_list: [],
      shop_types_selected: [],
      zone_list: [],
      zone_selected: [],
      distrib_list: [],
      distrib_selected: [],
      categories_list: [],
      categories_selected: [],
      brands_list: [],
      brands_selected: [],
      sku_list: [],
      sku_selected: [],

      group_name: '',
      group_id: ''
    },

    watch: {
      shop_types_filter: function(val) {
        if(!val)
          this.shopTypesClearAll()
      },

      zone_filter: function(val) {
        if(!val)
          this.zoneClearAll()
      },

      distrib_filter: function(val) {
        if(!val)
          this.distribClearAll()
      },

      categories_selected: function(val) {
        this.listBrands()
      },

      shop_types_selected: function() {
        this.listShops()
      },

      zone_selected: function() {
        this.listShops()
      },

      distrib_selected: function() {
        this.listShops()
      },

      min_sales: function() {
        this.listShops()
      },

      brands_selected: function() {
        this.listShops()
      },

      fromDate: function() {
        this.listShops()
      },

      toDate: function() {
        this.listShops()
      }
    },

    created: function() {
      this.listShopTypes()
      this.listZones()
      this.listDistrib()
      this.listCategories()
      this.listSku()
    },

    methods: {
      shopTypesSelectAll: function() {
        if(!this.shop_types_filter)
          return [];

        this.shop_types_selected = this.shop_types_list.map(function(type) {
          return type.id
        })
      },

      shopTypesClearAll: function() {
        this.shop_types_selected = []
      },

      zoneSelectAll: function() {
        if(!this.zone_filter)
          return []

        this.zone_selected = this.zone_list.map(function(type) {
          return type.id
        })
      },

      zoneClearAll: function() {
        this.zone_selected = []
      },

      distribSelectAll: function() {
        if(!this.distrib_filter)
          return []

        this.distrib_selected = this.distrib_list.map(function(type) {
          return type.id
        })
      },

      distribClearAll: function() {
        this.distrib_selected = []
      },

      categoriesSelectAll: function() {
        if(!this.category_filter)
          return []

        this.categories_selected = this.categories_list.map(function(type) {
          return type.id
        })
      },

      categoriesClearAll: function() {
        this.categories_selected = []
      },

      brandSelectAll: function() {
        if(!this.brand_filter)
          return []

        this.brands_selected = this.brands_list.map(function(type) {
          return type.id
        })
      },

      brandClearAll: function() {
        this.brands_selected = []
      },

      skuSelectAll: function() {

      },

      skuClearAll: function() {

      },

      listShopTypes: function() {
        var xhr = new XMLHttpRequest()
        var self = this
        xhr.open('GET', base_url+'shops/listShopsTypes')
        xhr.onload = function() {
          self.shop_types_list = JSON.parse(xhr.responseText)
        }
        xhr.send()
      },

      listZones: function() {
        var xhr = new XMLHttpRequest()
        var self = this
        xhr.open('GET', base_url+'shops/listZone')
        xhr.onload = function() {
          self.zone_list = JSON.parse(xhr.responseText)
        }
        xhr.send()
      },

      listDistrib: function() {
        var xhr = new XMLHttpRequest()
        var self = this
        xhr.open('GET', base_url+'shops/getDistributors')
        xhr.onload = function() {
          self.distrib_list = JSON.parse(xhr.responseText)
        }
        xhr.send()
      },

      listCategories: function() {
        var self = this
        jQuery.get(base_url+'shops/getCategories', function(data) {
          self.categories_list = data
        })
      },

      listBrands: function() {
        var self = this
        var param = jQuery.param({
          'cat_ids': this.categories_selected.join(',')
        })
        jQuery.get(base_url+'shops/getBrands?'+param, function(data) {
          self.brands_list = data
        })
      },

      listSku: function() {
        var self = this
        jQuery.get(base_url+'shops/getSku', function(data) {
          self.sku_list = data
        })
      },

      listShops: function() {
        var xhr = new XMLHttpRequest()
        var self = this
        xhr.open('GET', base_url+'shops/listShops?'+this.queryString())
        xhr.onload = function() {
          self.shopsList = JSON.parse(xhr.responseText)
        }
        xhr.send()
      },

      queryString: function() {
        return jQuery.param({
          'shop_types': this.shop_types_selected.join(','),
          'zone_types': this.zone_selected.join(','),
          'distributors': this.distrib_selected.join(','),
          'brands': this.brands_selected.join(','),
          'min_sales': this.min_sales,
          'from_date': this.fromDate,
          'to_date': this.toDate
        })
      },

      showShopInfo: function(shop) {
        this.shopPopup = shop;
        jQuery('.bs-shop-info-modal').modal({show: true})
      },

      modalGroupName: function() {
        if(this.selectedShops.length>0) {
            jQuery('.bs-group-name-modal').modal('show')
        }
      },

      groupSave: function() {
        if(this.group_name!='') {
          //jQuery('.bs-group-name-modal').modal('hide')
          var param = jQuery.param({
            'group_shops': this.selectedShops.map(function(shop){return shop.id}).join(','),
            'group_name': this.group_name
          })
          var self = this
          jQuery.get(base_url+'shops/saveGroup?'+param, function(data) {
            if(data['group_id']!=undefined) {
              self.group_id = data['group_id']
              self.modalIsNavigatToOffer()
            }
            else {
              MessageBox.Show(data.error)
            }
          })
        }
      },

      modalIsNavigatToOffer: function() {
        jQuery('.bs-offer-navigation-modal').modal('show')
      },

      closeOfferModal: function() {
        jQuery('.bs-offer-navigation-modal').modal('hide')
      },

      navigateToOffer: function() {
        document.location.href = base_url+'shops/assign_offer?group_id='+this.group_id
      },

      navigateToHome: function() {
        document.location.href = base_url+'shops'
      },
    },

    computed: {
      selectedShops: function() {
        return resultFilter.selected(this.shopsList)
      }
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.fromdate').datepicker({
      'autoclose': true,
      'format': 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
      if(typeof(ev.date)=='object')
      {
        shops.fromDate=ev.date.toJSON()
      }
      else
      {
        shops.fromDate=''
      }
    })
    $('.todate').datepicker({
      'autoclose': true,
      'format': 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
      // window.evd = ev.date
      if(typeof(ev.date)=='object')
      {
        shops.toDate=ev.date.toJSON()
      }
      else
      {
        shops.toDate=''
      }
    })
  })
</script>
