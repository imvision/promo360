<?php
class Shops_model extends CI_Model {

  public function getShopTypes()
  {
    $shopTypes = array();
    $query = $this->db->query("SELECT * FROM shop_type_ref");

    foreach ($query->result() as $row)
    {
      $shopTypes[] = array(
        'id' => $row->shop_type_id,
        'text' => $row->shop_type_code
      );
    }
    return $shopTypes;
  }

  public function getZone()
  {
    $zone = array();
    $query = $this->db->query("SELECT * FROM zone_ref");
    foreach($query->result() as $row)
    {
      $zone[] = array(
        'id' => $row->zone_id,
        'text' => $row->zone_title
      );
    }
    return $zone;
  }

  public function getDistributors()
  {
    $distrib = array();;
    $query = $this->db->query("SELECT * FROM distributor");
    foreach ($query->result() as $row) {
      $distrib[] = array(
        'id' => $row->dist_id,
        'text' => $row->dist_name
      );
    }
    return $distrib;
  }

  public function getCategories()
  {
    $categories = array();
    $query = $this->db->query("SELECT * FROM categories");
    foreach ($query->result() as $row)
    {
      $categories[] = array(
        'id' => $row->cat_id,
        'text' => $row->cat_title
      );
    }
    return $categories;
  }

  public function getBrands($filter = array())
  {
    $brands = array();
    // $query = $this->db->query('SELECT * FROM brands');
    $this->db->select('*');
    $this->db->from('brands');
    if(count($filter['cat_ids'])>0) {
        $this->db->where_in('brand_cat_id', $filter['cat_ids']);
    }
    $this->db->order_by('brand_cat_id');
    $query = $this->db->get();
    foreach ($query->result() as $row) {
      $brands[] = array(
        'id' => $row->brand_id,
        'text' => $row->brand_title
      );
    }
    return $brands;
  }

  public function getSku()
  {
    $sku = array();
    $query = $this->db->query('SELECT * FROM sku');
    foreach($query->result() as $row)
    {
      $sku[] = array(
        'id' => $row->sku_id,
        'text' => $row->sku_alias
      );
    }
    return $sku;
  }

  public function getShops($filter = array())
  {
    $shops = array();
    $this->db->select('sh.shop_id,sh.shop_name,sh.lat,sh.lng')
      ->select('st.shop_type_code,zr.zone_title,ds.dist_name')
      ->select('lc.district,lc.city,lc.state,lc.country')
      ->from('shops as sh')
      ->join('shop_type_ref as st', 'st.shop_type_id=sh.shop_type')
      ->join('zone_ref as zr', 'zr.zone_id=sh.zone')
      ->join('distributor as ds', 'ds.dist_id=sh.distributor')
      ->join('locations_uae as lc', 'lc.loc_id=sh.loc_id')
      ->where('inactive', 1);

    // Shop types
    if(count($filter['shop_types'])>0) {
        $this->db->where_in('shop_type', $filter['shop_types']);
    }

    // Zone types
    if(count($filter['zone_types'])>0) {
      $this->db->where_in('zone', $filter['zone_types']);
    }

    // Distributors
    if(count($filter['distrib'])>0) {
      $this->db->where_in('distributor', $filter['distrib']);
    }

    // Brands
    if(count($filter['brands'])>0) {
      $this->db->where_in('sl.brand_id', $filter['brands']);
    }

    $query = $this->db->get();
    foreach ($query->result() as $row)
    {
      $shops[] = array(
        'id' => $row->shop_id,
        'name' => $row->shop_name,
        'type' => $row->shop_type_code,
        'lat' => $row->lat,
        'lng' => $row->lng,
        'zone' => $row->zone_title,
        'distributor' => $row->dist_name,
        'district' => $row->district,
        'city' => $row->city,
        'state' => $row->state,
        'country' => $row->country,
        'sales_total' => 0,
        'selected' => true
      );
    }
    return $shops;
  }

  public function getShopsWithSales($filter = array())
  {
    $shops = array();
    $this->db->select('sh.shop_id,sh.shop_name,sh.lat,sh.lng')
      ->select('st.shop_type_code,zr.zone_title,ds.dist_name')
      ->select('lc.district,lc.city,lc.state,lc.country')
      ->select('SUM(sl.sale_v) as sales_total')
      ->from('shops as sh')
      ->join('shop_type_ref as st', 'st.shop_type_id=sh.shop_type')
      ->join('zone_ref as zr', 'zr.zone_id=sh.zone')
      ->join('distributor as ds', 'ds.dist_id=sh.distributor')
      ->join('locations_uae as lc', 'lc.loc_id=sh.loc_id')
      ->join('sales as sl', 'sl.shop_id=sh.shop_id')
      ->where('inactive', 1)
      ->group_by('sh.shop_id')
      ->having("sales_total > {$filter['min_sales']}");

    // Shop types
    if(count($filter['shop_types'])>0) {
        $this->db->where_in('shop_type', $filter['shop_types']);
    }

    // Zone types
    if(count($filter['zone_types'])>0) {
      $this->db->where_in('zone', $filter['zone_types']);
    }

    // Distributors
    if(count($filter['distrib'])>0) {
      $this->db->where_in('distributor', $filter['distrib']);
    }

    // Brands
    if(count($filter['brands'])>0) {
      $this->db->where_in('sl.brand_id', $filter['brands']);
    }

    if($filter['from_date']!='') {
      $this->db->where('sl.sale_date>=', $filter['from_date']);
    }

    if($filter['to_date']!='') {
      $this->db->where('sl.sale_date<=', $filter['to_date']);
    }

    $query = $this->db->get();
    foreach ($query->result() as $row)
    {
      $shops[] = array(
        'id' => $row->shop_id,
        'name' => $row->shop_name,
        'type' => $row->shop_type_code,
        'lat' => $row->lat,
        'lng' => $row->lng,
        'zone' => $row->zone_title,
        'distributor' => $row->dist_name,
        'district' => $row->district,
        'city' => $row->city,
        'state' => $row->state,
        'country' => $row->country,
        'sales_total' => $row->sales_total,
        'selected' => true
      );
    }
    return $shops;
  }

  public function insertGroup($group = array()) {
    $group_data = array(
      'group_name' => $group['group_name'],
      'created_on' => date('YYYY-MM-DD')
    );
    $this->db->insert('groups', $group_data);
    $group_id = $this->db->insert_id();

    $group_shops_data = array();
    foreach($group['shops'] as $shop_id) {
      $group_shops_data[] = array(
        'group_id' => $group_id,
        'shop_id' => $shop_id
      );
    }
    $this->db->insert_batch('group_shops', $group_shops_data);
    return $group_id;
  }

  public function insertOffer($offer= array(), $sku=array())
  {
    $this->db->insert('offers', $offer);
    $offer_id = $this->db->insert_id();
    $offer_sku = array();
    foreach($sku as $sku_id)
    {
      $offer_sku[] = array(
        'offer_id' => $offer_id,
        'sku_id' => $sku_id
      );
    }
    $this->db->insert_batch('offer_sku_cond', $offer_sku);
  }

  public function getGroups()
  {
    $groups = array();
    $query = $this->db->query("SELECT * FROM groups");
    foreach ($query->result() as $row)
    {
      $groups[] = array(
        'group_id' => $row->group_id,
        'group_name' => $row->group_name
      );
    }
    return $groups;
  }

  public function getOffers()
  {
    $offers = array();
    $query = $this->db->query('SELECT * FROM offers WHERE status IN ("active", "scheduled")');
    foreach($query->result() as $row)
    {
      $offers[] = array(
        'offer_id' => $row->offer_id,
        'offer_title' => $row->offer_title,
        'cond_value' => $row->cond_value
      );
    }
    return $offers;
  }

  public function getGroupShops($group_id)
  {
    $shop_ids = array();
    $query = $this->db->query("SELECT shop_id FROM group_shops WHERE group_id='$group_id'");
    foreach($query->result() as $row)
    {
      $shop_ids[] = $row->shop_id;
    }
    return $shop_ids;
  }

  public function saveOfferAssignments($data)
  {
    $offer_shops = array();
    foreach($data['groups'] as $group_id) {
      $shops = $this->getGroupShops($group_id);
      foreach($shops as $shop) {
        $offer_shops[] = array(
          'offer_id' => $data['offer_id'],
          'shop_id' => $shop,
          'availed_on' => date('Y-m-d')
        );
      }
    }
    $this->db->insert_batch('offer_shops', $offer_shops);
  }

  public function getMyShop($user_id)
  {
    $this->db->select('sh.shop_name, sh.shop_id, su.first_name')
      ->from('shops as sh')
      ->join('shop_users as su', 'su.shop_id=sh.shop_id')
      ->where('su.shop_user_id', $user_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function getMyLastOffer($shop_id)
  {
    $this->db->select('of.offer_id, of.offer_title, of.offer_text, of.description')
      ->from('offers as of')
      ->join('offer_shops as os', 'os.offer_id=of.offer_id')
      ->where('os.shop_id', $shop_id)
      ->order_by('os.pk_id', 'DESC')
      ->limit(1);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function getOffer($offer_id)
  {
    $this->db->select('of.*')
      ->from('offers as of')
      ->where('of.offer_id', $offer_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function getMyOffers($user_id, $status)
  {
    $offers = array();
    $this->db->select('of.offer_id, of.offer_title, of.end_date')
      ->from('offers as of')
      ->join('offer_shops as os', 'of.offer_id=os.offer_id')
      ->join('shop_users as su', 'os.shop_id=su.shop_id')
      ->where('su.shop_user_id', $user_id);
      if($status=='active') {
        $this->db->where('of.end_date >=', date('Y-m-d'));
      }
      else {
        $this->db->where('of.end_date <', date('Y-m-d'));
      }
      // ->where('of.status', $status);
    $query = $this->db->get();
    foreach ($query->result() as $row)
    {
      $offers[] = array(
        'offer_id' => $row->offer_id,
        'label' => $row->offer_title,
        'end' => $row->end_date
      );
    }
    return $offers;
  }

  public function getMySalesForOffer($shop_id, $offer_id)
  {
    $this->db->select('SUM(sl.sale_v) as total')
      ->from('sales sl')
      ->join('offer_sku_cond sk', 'sk.sku_id=sl.brand_id')
      ->where('sl.shop_id', $shop_id)
      ->where('sk.offer_id', $offer_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  public function getOfferSku($offer_id)
  {
    $sku = array();
    $this->db->select('cat_title, brand_title')
      ->from('offer_sku_cond sk')
      ->join('brands bn', 'sk.sku_id=bn.brand_id')
      ->join('categories ct', 'ct.cat_id=bn.brand_cat_id')
      ->where('offer_id', $offer_id);
      $query = $this->db->get();
      foreach ($query->result() as $row)
      {
        $sku[] = array(
          'cat' => $row->cat_title,
          'sku' => $row->brand_title
        );
      }
      return $sku;
  }
}
