var shopsApi = {
  listCategories: function(categories_list) {
    jQuery.get(base_url+'shops/getCategories', function(data) {
      data.map(function(item) {
        categories_list.push(item)
      })
    })
  },
  listBrands: function(categories_selected, brands_list) {
    var param = jQuery.param({
      'cat_ids': categories_selected
    })
    jQuery.get(base_url+'shops/getBrands?'+param, function(data) {
      data.map(function(item) {
        brands_list.push(item)
      })
    })
  }
}

var resultFilter = {
  all: function(shopsList) {
    return shopsList;
  },

  selected: function (shopsList) {
    return shopsList.filter(function (shop) {
      return shop.selected
    })
  },

  notSelected: function (shopsList) {
    return shopsList.filter(function (shop) {
      return !shop.selected
    })
  },
}
